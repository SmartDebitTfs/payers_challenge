﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayersPayment.Models
{
    public class PayerViewModelList
    {
        public IList<PayerViewModel> Items { get; set; }
    }
}
