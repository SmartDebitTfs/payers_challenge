﻿using PayersPayment.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayersPayment.Models
{
    public class PayerViewModel
    {
        public string Name { get; set; }
        public string Address { get; set;}
        public int  Age { get; set; }
        public GenderEnum Geneder { get; set; }
    }
}
