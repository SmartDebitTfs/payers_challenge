﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayersPayment.Interfaces
{
    public interface IRepositoryUpdate<T> where T : class, new()
    {
        void Update(T entity);
    }
}
