﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayersPayment.Interfaces
{
    public interface IRepositoryInsert<T> where T : class,new()
    {
        int Insert(T entity); 
    }
}
