﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayersPayment.Interfaces
{
    public interface IRepositoryGetOne<T> where T : class, new()
    {
        T Get(int id);
    }
}
