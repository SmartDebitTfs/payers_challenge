﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayersPayment.Interfaces
{
    public interface IRepositoryDelete<T> where T : class, new() 
    {
        void Delete(int id);
    }
}
