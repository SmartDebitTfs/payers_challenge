﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayersPayment.Interfaces
{
    public interface IRepositoryGet<T> where T: class, new()
    {
        IEnumerable<T> Get();
    }
}
