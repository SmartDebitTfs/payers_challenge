﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayersPayment.Interfaces
{
    public interface IRepository<T> : IRepositoryGet<T>, IRepositoryUpdate<T> where T : class,new() 
    {

    }
}
