﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PayersPayment.Enums;

namespace PayersPayment.Data.Entities
{
    public class Payer
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public int Age { get; set; }
        public GenderEnum Geneder { get; set; }
    }
}
