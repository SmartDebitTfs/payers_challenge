﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayersPayment.Data.Entities
{
    public class Invoice
    {
        public int IncoiceId { get; set; }
        public Dictionary<string,double> Items { get; set; }
        public DateTime InvoiceDate { get; set; }
        public Payer Payer { get; set; }
        public Payment Payment { get; set; }
    }
}
