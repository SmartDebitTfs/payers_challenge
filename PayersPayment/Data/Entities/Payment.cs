﻿using PayersPayment.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayersPayment.Data.Entities
{
    public class Payment
    {
        public DateTime Date { get; set; }
        public bool IsComplete { get; set; }

        public PaymentMethodEnum PaymentMethod {get;set;}

    }
}
