﻿using PayersPayment.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PayersPayment.Data.Entities;

namespace PayersPayment.Data
{
    public class PayerRepository : IRepositoryGet <Payer>
    {
        private static List<Payer> _payers { get; set; } 

        public PayerRepository()
        {
            if (_payers == null)
            {
                _payers = new List<Payer>() { 
                    new Payer { Address = "123 London street", Age = 34, Geneder = Enums.GenderEnum.Female, Name = "Jasmine" },
                    new Payer { Address = "1 Book street", Age = 41, Geneder = Enums.GenderEnum.Female, Name = "Phonix" },
                    new Payer { Address = "77 Trash Avenue", Age = 26, Geneder = Enums.GenderEnum.Male, Name = "Thomas" },
                    new Payer { Address = "21 Board street", Age = 54, Geneder = Enums.GenderEnum.Male, Name = "Jhon" },
                    new Payer { Address = "5 Heigh street", Age = 63, Geneder = Enums.GenderEnum.Female, Name = "Brigitte" },
                    new Payer { Address = "10 Down street", Age = 30, Geneder = Enums.GenderEnum.Male, Name = "George" },
                    new Payer { Address = "3 Rose close", Age = 18, Geneder = Enums.GenderEnum.Female, Name = "Vivienne" },
                };
                
            }
        }

        public IEnumerable<Payer> Get()
        {
            return _payers;
        }
    }
}
