﻿using PayersPayment.Data.Entities;
using PayersPayment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayersPayment.Services
{
    public static class PayerMapper
    {
        public static PayerViewModel Map(this Payer payer)
        {
            return new PayerViewModel
            {
                Name = payer.Name,
                Address = payer.Address,
                Age = payer.Age,
                Geneder = payer.Geneder
            };
        }

        public static Payer Map(this PayerViewModel payer)
        {
            return new Payer
            {
                Name = payer.Name,
                Address = payer.Address,
                Age = payer.Age,
                Geneder = payer.Geneder
            };
        }
    }
}
