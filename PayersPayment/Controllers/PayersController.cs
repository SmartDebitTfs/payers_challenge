﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PayersPayment.Interfaces;
using PayersPayment.Models;
using PayersPayment.Data.Entities;
using PayersPayment.Services;
using System.Web.Mvc;
using System.Net;
using PayersPayment.Data;


namespace PayersPayment.Controllers
{
    public class PayersController : System.Web.Mvc.Controller
    {
        private readonly ILogger<PayersController> _logger;
        private IRepositoryGet<Payer> _repository;

        public PayersController(ILogger<PayersController> logger, IRepositoryGet<Payer> repository)
        {
            _logger = logger;
            _repository = repository;
        }

        public ActionResult Index()
        {
            PayerViewModelList model = new PayerViewModelList { Items = new List<PayerViewModel> { } };

            model.Items = _repository.Get().SelectMany(i => new List<PayerViewModel> { i.Map() }).ToList(); 
            
            return View(model);
        }

        public ActionResult Edit(string name)
        {
            PayerRepository repo = new PayerRepository();
            var payerlist = repo.Get();
            var payer = payerlist.Where(p => p.Name == name).FirstOrDefault();
            return View(name);
        }
    }
}
