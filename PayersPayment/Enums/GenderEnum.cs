﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayersPayment.Enums
{
    public enum GenderEnum
    {
        Male = 1,
        Female = 2
    }
}
