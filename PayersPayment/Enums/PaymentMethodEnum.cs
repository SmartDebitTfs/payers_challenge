﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayersPayment.Enums
{
    public enum PaymentMethodEnum
    {
        DebitCard = 1,
        CreditCard = 2,
        BankTransfer = 3,
        DirectDebit = 4,
    }
}
